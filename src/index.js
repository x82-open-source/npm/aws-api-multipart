import Busboy from 'busboy';
import getStream from 'get-stream';
/**
 * Parse and convert the raw multipart string
 * @method parse
 * @param  {object} req
 * @return {Promise<object>}
 */
export const parse = async req => await new Promise((resolve, reject) => {
	const encoding = req.isBase64Encoded ? 'base64' : 'utf-8',
		ret = {
			files: [],
			fields: {}
		},
		busboy = new Busboy({
			headers: req.headers
		});
	busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
		getStream.buffer(file).then((buffer) => {
			ret.files.push({
				fieldName: fieldname,
				fileName: filename,
				encoding: encoding,
				mimetype: mimetype,
				/**
				 * The file as a stream
				 * @type {Stream}
				 */
				data: buffer
			});
		});
	});
	busboy.on('field', (fieldname, val) => {
		ret.fields[fieldname] = val;
	});
	busboy.on('finish', () => resolve(ret));
	busboy.on('error', reject);
	busboy.write(req.body, encoding);
});
