const aliases = require('../config/aliases');

module.exports = {
    coverageDirectory: '<rootDir>/coverage',
    collectCoverage: true,
    moduleNameMapper: {
        ...aliases.forJest
    },
    collectCoverageFrom: ['<rootDir>/src/**/*.{js,jsx}'],
    testPathIgnorePatterns: ['tests/setup'],
    globals: {
        __PATH_PREFIX__: ''
    },
    coverageReporters: ['cobertura'],
    testURL: 'http://localhost',
    rootDir: '../'
};