const aliases = require('./config/aliases');
module.exports = function (api) {

  let presets = [
    ["@babel/preset-env",
      {
        "targets": {
          "node": "10"
        }
      }]
  ]

  if (api.env("production")) {
    presets.push(['minify', { builtIns: false }])
  }

  api.cache(false);

  return {
    "presets": presets,
    "plugins": [["module-resolver", aliases.forBabel]]
  }
}



